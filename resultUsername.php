<?php

error_reporting(E_ALL);
ini_set("display_errors","On");
require "framework/header.php";
require "framework/conn.php";
require "php/databaseUser.php";
require "php/resultCalculator.php";
$r = new resultCalculator($db, $_POST);
$username = $_GET['username'];
$userID = $r->getUserID($username);

if($userID == NULL) { ?>

    <h1> Deze username is niet bekend.</h1>
    <a class="center" href="more.php"><p class="center intro">Klik hier om terug te gaan</p></a>

<?php }
else {

    $r->setPost($r->getUserID($username));

?>


    <h1>
        Uw resultaten
    </h1>

    <p>
        Scroll naar beneden voor een overzicht van alle vragen.
    </p>
    <br />

    <table class="center results">
        <?php $r->echoResultParties(); ?>
    </table>

    <hr class="divider">

    <h1>Bij welke vraag was u het eens met welke partij</h1>

    <p>
        Houdt uw muis over de afkorting van een partij om te zien wat zij geantwoord hebben op de stelling, en welke toelichting zij bij hun antwoord hebben gegeven.
    </p>

    <table class="center results">
        <tr>
            <th class="results"><h3>Vraag</h3></th>
            <th class="results">Partij waar je het mee eens bent</th>
            <th class="results">Partij waar je het mee oneens bent</th>
        </tr>

        <?php $r->echoDetailedBreakdown();?>

    </table>

<?php } ?>