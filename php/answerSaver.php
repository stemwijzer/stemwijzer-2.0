<?php

class answerSaver extends databaseUser
{
    public $db;
    public $post;
    public $username;

    function __construct($db, $post) {
        $this->db = $db;
        $this->post = $post;
        $this->username = $this->db->real_escape_string($this->post['username']);
    }

    function processNewAnswers() {
        $userID = $this->getUserID($this->username);

        if($userID == NULL) {
            $userID = $this->saveNewUser();
            $this->saveNewUserAnswers($userID);
        }
        else {
            //$this->deleteUserAnswers($userID);
            $this->updateNewUserAnswers($userID);
        }
    }

    function saveNewUserAnswers($userID) {
        $amountOfQuestions = $this->getAmountOfQuestions();

        for($q = 1; $q <= $amountOfQuestions; $q++){
            $querySaveAnswer = 'INSERT INTO `antwoord-user`(`userID`, `vraagID`, `antwoordID`, `belang`) VALUES ('.$userID.','.$q.','.$this->db->real_escape_string($_POST['v'.$q]).', '.$this->db->real_escape_string($_POST['b'.$q]).')';
            $resultSaveAnswer = $this->db->query($querySaveAnswer);

            if($resultSaveAnswer == FALSE) {
                echo "ERROR in answerSaver, couldn't insert new answer.";
            }
        }
    }

    function updateNewUserAnswers($userID) {
        $amountOfQuestions = $this->getAmountOfQuestions();

        for($q = 1; $q <= $amountOfQuestions; $q++){
            $queryUpdateAnswer = 'UPDATE `antwoord-user` SET `antwoordID` = '.$this->db->real_escape_string($_POST['v'.$q]).', `belang` = '.$this->db->real_escape_string($_POST['b'.$q]).' WHERE `userID` = '.$userID.' AND `vraagID` = '.$q;
            $resultUpdateAnswer = $this->db->query($queryUpdateAnswer);

            if($resultUpdateAnswer == FALSE) {
                echo "ERROR in answerSaver, couldn't update new answer.";
            }
        }
    }

    function deleteUserAnswers($userID) {
        $queryDeleteOldAnswers = 'DELETE FROM `antwoord-user` WHERE `userID` = '.$userID;
        $resultDeleteOldAnswers = $this->db->query($queryDeleteOldAnswers);

        if($resultDeleteOldAnswers == FALSE) {
            echo "ERROR in answerSaver, couldn't delete old answers";
        }
    }

    function saveNewUser() {
        $queryMakeNewUser = 'INSERT INTO `user` (`username`) VALUES ("'.$this->username.'") ';
        $resultMakeNewUser = $this->db->query($queryMakeNewUser);

        if($resultMakeNewUser == FALSE) {
            echo "ERROR in answerSaver, couldn't insert new user.";
            return NULL;
        }
        else {
            return $this->getUserID($this->username);
        }
    }

    function savePartyAnswers($partyID)
    {
        $amountOfQuestions = $this->getAmountOfQuestions();
        $resultSaveAnswer = false;

        for ($q = 1; $q <= $amountOfQuestions; $q++) {
            echo $this->db->real_escape_string($_POST['toelichtingv' . $q]);
            $querySaveAnswer = 'UPDATE `antwoord-partij` SET `antwoordID` = '.$this->db->real_escape_string($_POST['v' . $q]).', `toelichting` = "'.$this->db->real_escape_string($_POST['toelichtingv' . $q]).'" WHERE `partijID` = '.$partyID.' AND `vraagID` = '.$q;
            $resultSaveAnswer = $this->db->query($querySaveAnswer);
        }
        if($resultSaveAnswer == FALSE) {
            echo $this->db->real_escape_string($_POST['toelichtingv1']);
            echo $this->db->real_escape_string($_POST['toelichtingv' . $q]);
            echo "ERROR in answerSaver, couldn't insert new answer.";
            return false;
        }
        else {
            return true;
        }
    }

    function deletePartyAnswers($partyID) {
        $queryDeleteOldAnswers = 'DELETE FROM `antwoord-partij` WHERE `partijID` = '.$partyID;
        $resultDeleteOldAnswers = $this->db->query($queryDeleteOldAnswers);

        if($resultDeleteOldAnswers == FALSE) {
            echo "ERROR in answerSaver, couldn't delete old answers";
            return false;
        }
        else {
            return true;
        }
    }
}