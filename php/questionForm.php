<?php
class questionForm extends databaseUser
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    function echoForm() {
        $questions = $this->getQuestions();
        foreach ($questions as $question) {
            $questionID = $question['vraagID'];

            echo '<h2 class="center">'.$question['vraagTekst'].'</h2>';
            echo '<p class="center">'.$question['description'].'</p>';

            $answers = $this->getAnswersForQuestion($questionID);
            foreach ($answers as $answer) {
                echo
                    '
                        <input class="center" required id="v' .$questionID. 'a' .$answer['antwoordID']. '" type="radio" name="v' .$questionID. '" value = "' .$answer['antwoordID']. '">
                        
                        <label class="center" for="v' .$questionID. 'a' .$answer['antwoordID']. '">
                            ' .$answer['antwoordTekst']. '
                        </label>
                    ';

            }

            echo
                '
                    <h3 class="center">
                        Hoe belangrijk is dit voor u?
                    </h3>
                    
                    <p class="center">
                        Niet belangrijk
                        
                        <input class="center" type="range" name="b' .$questionID. '" min="1" max="3" value="2">
                    
                        Erg belangrijk
                    </p>
                    
                    <hr class="divider">
            ';
        }
    }
}