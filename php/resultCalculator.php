<?php

class resultCalculator extends databaseUser
{
    public $db;
    public $post;
    public $partyScore;

    function __construct($db, $post)
    {
        $this->db = $db;
        $this->post = $post;
    }

    function echoResultParties() {
        $this->createPartyScore();
        $this->scoreParties();
        list($partyID, $scoreOfParty, $maxScore) = $this->sortPartyScore();

        foreach($partyID as $key => $party) {
            $partyName = $this->getPartyName($party);

            echo'<tr>
                        <th class="results"><img class="partyLogo" src="../img/partyLogo/'.$partyName.'.png"></th>
                        <th class="results">'.round(($scoreOfParty[$key]/$maxScore[$key])*100).' %</th>
                     </tr>';
        }
    }

    public function echoDetailedBreakdown() {
        $questions = $this->getQuestions();
        foreach($questions as $question) {
            $answers = $this->getAnswerAllParties($question['vraagID']);
            echo'<tr>
                    <th class="results">'.$question['vraagTekst'].'</th>
                    <th class="results">';
            foreach ($answers as $answer) {
                $userAnswerID = $this->db->real_escape_string($this->post['v'.$question['vraagID']]);
                $partyName = $this->getPartyName($answer['ID']);
                if($answer['antwoordID'] == $userAnswerID) {
                    echo '<div class="tooltip">'.$partyName.'<span class="tooltiptext">'.$answer['antwoordTekst'].', '.$answer['toelichting'].'</span></div><br />';
                }
            }
            echo'  </th>
                    <th class="results">';
            foreach ($answers as $answer) {
                $userAnswerID = $this->db->real_escape_string($this->post['v'.$question['vraagID']]);
                $partyName = $this->getPartyName($answer['ID']);
                if($answer['antwoordID'] != $userAnswerID) {
                    echo '<div class="tooltip">'.$partyName.'<span class="tooltiptext">'.$answer['antwoordTekst'].', '.$answer['toelichting'].'</span></div><br />';
                }
            }
            echo'    </th>
                 </tr>  ';
        }
    }


    function createPartyScore() {
        $amountOfParties = $this->getAmountOfParties();

        for($i = 1; $i <= $amountOfParties; $i++) {
            $this->partyScore[$i] = array($i, 0, 0);
        }
    }

    function createUserScore() {
        $amountOfUsers = $this->getAmountOfUsers();
        $lowestUserID = $this->getLowestUserID();

        for($i = $lowestUserID; $i <= $amountOfUsers+$lowestUserID; $i++) {
            $this->partyScore[$i] = array($i, 0);
        }
        //var_dump($this->partyScore);
    }

    function sortPartyScore() {
        $partyID = NULL;
        $scoreOfParty = NULL;
        $maxScore = NULL;
        foreach ($this->partyScore as $key => $row) {
            $partyID[$key] = $row[0];
            $scoreOfParty[$key] = $row[1];
            $maxScore[$key] = $row[2];
        }
        array_multisort($scoreOfParty, SORT_DESC, SORT_NUMERIC, $partyID, $maxScore);

        return [$partyID, $scoreOfParty, $maxScore];
    }

    function scoreParties() {
        $amountOfQuestions = $this->getAmountOfQuestions();

        for($i = 1; $i <= $amountOfQuestions; $i++) {
            $this->scoreQuestion($i, $this->getAnswerAllParties($i));
        }

    }

    function scoreUsers() {
        $amountOfQuestions = $this->getAmountOfQuestions();

        for($i = 1; $i <= $amountOfQuestions; $i++) {
            $this->scoreQuestion($i, $this->getAnswerAllUsers($i));
        }

    }

    function setPost($userID) {
        $queryUserAnswers = "SELECT `vraagID`, `antwoordID`, `belang` FROM `antwoord-user` WHERE  userID = $userID";
        $resultUserAnswers = $this->db->query($queryUserAnswers);

        while ($row = $resultUserAnswers->fetch_assoc()) {
            $this->post['v'.$row['vraagID']] = $row['antwoordID'];
            $this->post['b'.$row['vraagID']] = $row['belang'];
        }
    }

    function scoreQuestion($questionID, $answers) {
        $userAnswerID = $this->db->real_escape_string($this->post['v'.$questionID]);
        $userAnswerType = $this->getAnswerType($questionID, $userAnswerID);
        $weight = $this->db->real_escape_string($this->post['b'.$questionID]);
        foreach ($answers as $answer) {
            $partyID = $answer['ID'];

            //Exact answer
            if($userAnswerID == $answer['antwoordID']) {
                if($weight == 1) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + (0.5 * 1.2);
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + (0.5 * 1.2);
                }
                else if($weight == 2) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + (1 * 1.2);
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + (1 * 1.2);
                }
                else if($weight == 3) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + (1.5 * 1.2);
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + (1.5 * 1.2);
                }
            }

            //Similar answer
            else if($userAnswerType == $answer['antwoordSoort']) {
                if($weight == 1) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + 0.5;
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + 0.5;
                }
                else if($weight == 2) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + 1;
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + 1;
                }
                else if($weight == 3) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + 1.5;
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + 1.5;
                }
            }

            //Wrong answer
            else {
                if($weight == 1) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + 0;
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + 0.5;
                }
                else if($weight == 2) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + -0.25;
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + 1;
                }
                else if($weight == 3) {
                    $this->partyScore[$partyID][1] = $this->partyScore[$partyID][1] + -0.5;
                    $this->partyScore[$partyID][2] = $this->partyScore[$partyID][2] + 1.5;
                }
            }
        }
    }

    function getAnswerAllUsers($questionID) {
        $answers = NULL;
        $queryGetAnswersUsers = 'SELECT AU.userID as ID, A.antwoordSoort, AU.antwoordID FROM `antwoord` A INNER JOIN `antwoord-user` AU ON A.antwoordID = AU.antwoordID AND A.vraagID = AU.vraagID WHERE  A.vraagID = '.$questionID.' ORDER BY AU.userID';
        $resultGetAnswersUsers = $this->db->query($queryGetAnswersUsers);

        while ($row = $resultGetAnswersUsers->fetch_assoc()) {
            $answers[] = $row;
        }
        return $answers;
    }

    function getAnswerAllParties($questionID) {
        $answers = NULL;
        $queryGetAnswersParties = 'SELECT AV.partijID as ID, A.antwoordSoort, AV.antwoordID, AV.toelichting, A.antwoordTekst FROM `antwoord` A INNER JOIN `antwoord-partij` AV ON A.antwoordID = AV.antwoordID AND A.vraagID = AV.vraagID WHERE  A.vraagID = '.$questionID.' ORDER BY AV.partijID';
        $resultGetAnswersParties = $this->db->query($queryGetAnswersParties);

        while ($row = $resultGetAnswersParties->fetch_assoc()) {
            $answers[] = $row;
        }
        return $answers;
    }


}