<?php

abstract class databaseUser
{
    public $db;

    function getUserID($username)
    {
        $queryGetUserID = 'SELECT userID FROM user WHERE username = "' . $this->db->real_escape_string($username) . '"';
        $resultGetUserID = $this->db->query($queryGetUserID);

        if ($resultGetUserID != FALSE) {
            while ($row = $resultGetUserID->fetch_assoc()) {
                $userID = $row['userID'];
            }
        }

        if (ISSET($userID)) {
            return $userID;
        } else {
            return NULL;
        }
    }

    function getUserName($userID)
    {
        $queryGetUsername = 'SELECT username FROM user WHERE userID = "' . $this->db->real_escape_string($userID) . '"';
        $resultGetUsername = $this->db->query($queryGetUsername);

        if ($resultGetUsername != FALSE) {
            while ($row = $resultGetUsername->fetch_assoc()) {
                $username = $row['username'];
            }
        }

        if (ISSET($username)) {
            return $username;
        } else {
            return NULL;
        }
    }

    function getPartyName($partyID)
    {
        try {
            $queryGetPartyName = 'SELECT partijNaam FROM partij WHERE partijID = ' . $partyID;
            $resultGetPartyName = $this->db->query($queryGetPartyName);

            if ($resultGetPartyName != FALSE) {
                while ($row = $resultGetPartyName->fetch_assoc()) {
                    $partyName = $row['partijNaam'];
                }
            }

            if (ISSET($partyName)) {
                return $partyName;
            } else {
                throw new Exception('No party name found for partyID ' . $partyID);
            }
        } catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }

    function getPartyID($partyName)
    {
        try {
            $queryGetPartyID = 'SELECT partijID FROM partij WHERE partijNaam = ' . $partyName;
            $resultGetPartyID = $this->db->query($queryGetPartyID);

            if ($resultGetPartyID != FALSE) {
                while ($row = $resultGetPartyName->fetch_assoc()) {
                    $partyName = $row['partijNaam'];
                }
            }

            if (ISSET($partyName)) {
                return $partyName;
            } else {
                throw new Exception('No party name found for partyID ' . $partyID);
            }
        } catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }

    function getAnswerType($questionID, $answerID)
    {
        try {
            $queryGetAnswerType = 'SELECT A.antwoordSoort FROM `antwoord` A WHERE  A.vraagID = ' . $questionID . ' AND A.antwoordID = ' . $answerID;
            $resultGetAnswerType = $this->db->query($queryGetAnswerType);

            if ($resultGetAnswerType != FALSE) {
                while ($row = $resultGetAnswerType->fetch_assoc()) {
                    $answerType = $row['antwoordSoort'];
                }
            }

            if (ISSET($answerType)) {
                return $answerType;
            } else {
                throw new Exception('No answerType was found for question ' . $questionID);
            }
        } catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }

    }

    function getAmountOfParties()
    {
        try {
            $queryAmountOfParties = "SELECT count(partijID) AS nr FROM partij";
            $resultAmountOfParties = $this->db->query($queryAmountOfParties);

            if ($resultAmountOfParties != FALSE) {
                while ($row = $resultAmountOfParties->fetch_assoc()) {
                    $amountOfParties = $row['nr'];
                }
            }

            if (ISSET($amountOfParties)) {
                return $amountOfParties;
            } else {
                throw new Exception("No parties found.");
            }
        } catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }


    function getAmountOfUsers()
    {
        try {
            $queryAmountOfUsers = "SELECT count(userID) AS nr FROM user";
            $resultAmountOfUsers = $this->db->query($queryAmountOfUsers);

            if ($resultAmountOfUsers != FALSE) {
                while ($row = $resultAmountOfUsers->fetch_assoc()) {
                    $amountOfUsers = $row['nr'];
                }
            }

            if (ISSET($amountOfUsers)) {
                return $amountOfUsers;
            } else {
                throw new Exception("No users found.");
            }
        } catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }

    function getAmountOfQuestions()
    {
        try {
            $queryAmountOfQuestions = "SELECT count(vraagID) AS nr FROM vraag";
            $resultAmountOfQuestions = $this->db->query($queryAmountOfQuestions);

            if ($resultAmountOfQuestions != FALSE) {
                while ($row = $resultAmountOfQuestions->fetch_assoc()) {
                    $amountOfQuestions = $row['nr'];
                }
            }

            if (ISSET($amountOfQuestions)) {
                return $amountOfQuestions;
            } else {
                throw new Exception('No questions found.');
            }
        } catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }

    function getLowestUserID()
    {
        try {
            $queryLowestUserID = "SELECT min(userID) AS nr FROM user";
            $resultLowestUserId = $this->db->query($queryLowestUserID);

            if ($resultLowestUserId != FALSE) {
                while ($row = $resultLowestUserId->fetch_assoc()) {
                    $lowestUserID = $row['nr'];
                }
            }

            if (ISSET($lowestUserID)) {
                return $lowestUserID;
            } else {
                throw new Exception("Couldn't find lowest userID");
            }
        } catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }

    function getQuestions()
    {
        try {
            $queryGetQuestions = 'SELECT V.vraagID, V.vraagTekst, V.description FROM vraag V';
            $resultGetQuestions = $this->db->query($queryGetQuestions);
            $questions = NULL;

            if ($resultGetQuestions->num_rows > 0) {
                while ($row = $resultGetQuestions->fetch_assoc()) {
                    $questions[] = $row;
                }
            }
            if(ISSET($questions)) {
                return $questions;
            }
            else {
                throw new Exception("Couldn't get questions from database");
            }
        }
        catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }

    function getQuestion($questionID)
    {
        try {
            $queryGetQuestion = 'SELECT V.vraagID, V.vraagTekst, V.description FROM vraag V WHERE V.vraagID = '.$questionID;
            $resultGetQuestion = $this->db->query($queryGetQuestion);
            $question = NULL;

            if ($resultGetQuestion->num_rows > 0) {
                while ($row = $resultGetQuestion->fetch_assoc()) {
                    $question[] = $row;
                }
            }
            if(ISSET($question)) {
                return $question;
            }
            else {
                throw new Exception("Couldn't get question from database");
            }
        }
        catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }

    function getAnswersForQuestion($id)
    {
        try {
            $queryGetAnswersForQuestion = 'SELECT A.antwoordTekst, A.antwoordID FROM antwoord A WHERE vraagID = ' . $id . ' ORDER BY antwoordTekst ASC';
            $resultGetAnswersForQuestion = $this->db->query($queryGetAnswersForQuestion);
            $answers = NULL;

            if ($resultGetAnswersForQuestion->num_rows > 0) {
                while ($row = $resultGetAnswersForQuestion->fetch_assoc()) {
                    $answers[] = $row;
                }
            }

            if (ISSET($answers)) {
                return $answers;
            } else {
                throw new Exception("Couldn't get answers to this question from database");
            }
        }
        catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }
    }

    function getPartyAnswer ( $questionID, $partyID ) {
        try {
            $queryGetPartyAnswer = 'SELECT AP.antwoordID AS partyAnswer FROM antwoord A INNER JOIN `antwoord-partij` AP ON A.vraagID = AP.vraagID WHERE A.vraagID = '.$questionID.' AND AP.partijID = '.$partyID.' ORDER BY A.antwoordTekst ASC ';
            $resultGetPartyAnswer = $this->db->query($queryGetPartyAnswer);
            $answer = NULL;

            if ($resultGetPartyAnswer->num_rows > 0) {
                while ($row = $resultGetPartyAnswer->fetch_assoc()) {
                    $answer = $row['partyAnswer'];
                }
            }

            if (ISSET($answer)) {
                return $answer;
            } else {
                throw new Exception("Couldn't get answers to this question from database");
            }
        }
        catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }

    }

    function getPartyToelichting ( $questionID, $partyID ) {
        try {
            $queryGetPartyToelichting = 'SELECT AP.toelichting AS toelichting FROM antwoord A INNER JOIN `antwoord-partij` AP ON A.vraagID = AP.vraagID WHERE A.vraagID = '.$questionID.' AND AP.partijID = '.$partyID.' ORDER BY A.antwoordTekst ASC ';
            $resultGetPartyToelichting = $this->db->query($queryGetPartyToelichting);
            $answer = NULL;

            if ($resultGetPartyToelichting->num_rows > 0) {
                while ($row = $resultGetPartyToelichting->fetch_assoc()) {
                    $answer = $row['toelichting'];
                }
            }

            if (ISSET($answer)) {
                return $answer;
            } else {
                throw new Exception("Couldn't get toelichting to this question from database");
            }
        }
        catch (Exception $e) {
            echo '<br /><br /><b>An error occured with the database, please alert <a href="mailto:info@th8.nl">info@th8.nl</a> with the following error message: ' . $e->getMessage();;
            exit();
        }

    }
}