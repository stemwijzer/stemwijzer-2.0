<?php

error_reporting(E_ALL);
ini_set("display_errors","On");
require "framework/header.php";
require "framework/conn.php";
require "php/databaseUser.php";
require "php/answerSaver.php";
require "php/resultCalculator.php";

if(0 < strlen($_POST['username'])) {
    $s = new answerSaver($db, $_POST);
    $s->processNewAnswers();
}
$r = new resultCalculator($db, $_POST);

?>

        <h1>
            Uw resultaten
        </h1>

        <p>
            Scroll naar beneden voor een overzicht van alle vragen.
        </p>
        <br />

        <table class="center results">
            <?php $r->echoResultParties(); ?>
        </table>

        <hr class="divider">

        <h1>Bij welke vraag was u het eens met welke partij</h1>

        <p>
            Houdt uw muis over de afkorting van een partij om te zien wat zij geantwoord heeft op de stelling, en welke toelichting zij bij haar antwoord heeft gegeven.
        </p>

        <table class="center results">

            <tr>
                <th class="results"><h3>Vraag</h3></th>
                <th class="results">Partij waar je het mee eens bent</th>
                <th class="results">Partij waar je het mee oneens bent</th>
            </tr>

            <?php $r->echoDetailedBreakdown();?>

        </table>
