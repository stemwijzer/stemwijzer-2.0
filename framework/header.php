<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../css/kieswijzer-basestyle.css">
    <link rel="stylesheet" href="../css/kieswijzer-questions.css">
    <link rel="stylesheet" href="../css/kieswijzer-forms.css">
    <link rel="stylesheet" href="../css/kieswijzer-admin.css">
    <link rel="stylesheet" href="../css/kieswijzer-result.css">
    <meta name="description" content="Officiële RMTK Stemwijzer">
    <meta charset="utf-8">
    <meta name=viewport content="width=device-width, initial-scale=0.5">
    <title>RMTK Stemwijzer</title>

</head>
<body>

<?php
    include "./framework/browser.php";
    include_once("./framework/analyticstracking.php");
?>

    <div class="container">
        <div class="bannerImg">
            <div class="banner">
                <a href="../index.php"><img src="../img/banner.png" alt="banner"></a>
            </div>
            <div class="menu">
                <ul>
                    <li><a href="../index.php" >                    Home			    </a></li>
                    <li><a href="https://motie.th8.nl">				Motiegenerator      </a></li>
                    <li><a href="../more.php">                      Meer functies       </a></li>
                    <li><a href="../admin/admin.php">				Beheer      	    </a></li>
                </ul>
            </div>
        </div>
        <div class="content">