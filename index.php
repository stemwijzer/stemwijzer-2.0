<?php
error_reporting(E_ALL);
ini_set("display_errors","On");
require "php/databaseUser.php";
require "framework/header.php";
require "framework/conn.php";
require "php/questionForm.php";

?>

            <form class="formstyle" action="result.php" method="post">
                <h1 class="center">
                    Stemwijzer
                </h1>

                <p class="center intro">
                    Vul een gebruikersnaam in om je resultaat op te slaan en te kunnen vergelijken met andere gebruikers. <br />
                    Je kunt het veld leeg laten als je jouw resultaat anoniem wilt houden.
                </p>

                <div class="center">
                    <label class="intro" for="username">
                        Gebruikersnaam
                    </label>
                    <br />
                    <input class="username-field" type="text" id="username" name="username" maxlength="20">
                </div>

                <hr class="divider">

                <?php
                    $q = new questionForm($db);
                    $q->echoForm();
                ?>

                <h2>
                    Klaar
                </h2>

                <input class="center questions" type="submit" value="Bekijk resultaat">

            </form>