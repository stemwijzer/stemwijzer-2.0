<?php require "framework/header.php"; ?>

    <h1>
        Bekijk uw resultaat
    </h1>
    <p class="intro">
        Hiermee haalt u uw eerder ingevulde test op.
    </p>
    <form class="center formstyle" method="get" action="resultUsername.php">
        <input type="text" name="username" maxlength="20" title="Gebruikersnaam">
        <input class="center function" type="submit" value="Bekijk resultaat">
    </form>

