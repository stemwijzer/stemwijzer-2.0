<?php
error_reporting(E_ALL);
ini_set("display_errors","On");
require "framework/header.php";
require "framework/conn.php";
require "php/databaseUser.php";
require "php/resultCalculator.php";
$r = new resultCalculator($db, $_POST);
$username = $_GET['username'];
$userID = $r->getUserID($username);

if($userID == NULL) { ?>

<h1>Deze username is niet bekend.</h1>
<a class="center" href="more.php"><p class="center intro">Klik hier om terug te gaan</p></a>

<?php }
else {
    $r->setPost($r->getUserID($username));
    ?>

<h1>
    Uw resultaten
</h1>

<table class="center results">
    <?php $r->echoResultUsers(); ?>
</table>
<?php } ?>